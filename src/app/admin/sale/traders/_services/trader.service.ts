import { Injectable } from '@angular/core';
import { Trader } from '../_models/trader';

@Injectable()
export class TraderService {
    public traders: Trader[] = [
        new Trader(1, 'hero', 'hero', 'hero@hero.com', '21231231', '2131213', 'pass'),
        new Trader(2, 'alfa', 'alfa', 'alfa@alfa.com', '21231231', '2131213', 'pass'),
        new Trader(3, 'ranger', 'ranger', 'ranger@hero.com', '21231231', '2131213', 'pass'),
    ]
    constructor() { }

    getTraderById(id: number): Trader{
        return this.traders.find(x => x.traderID == id);
    }

}