import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../../shared/_services/shared.service';
import { StocksService } from '../_services/stocks.service';
import { Stock } from '../_models/stock';

@Component({
    templateUrl: 'admin.modify-stock-sale.component.html'
})

export class AdminModifyStockSaleComponent {
        constructor(
            private translate: TranslateService,
            private sharedService: SharedService,
            private route: ActivatedRoute,
            private stocksService: StocksService,
        ) { }
        public stock: Stock;
        private stockID: number;
    
        ngOnInit() {
            //language
            let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
            this.translate.use(lang);
            this.route.params.subscribe(
                (params: any) => {
                    this.stockID = params['id']
                    //get static page
                    if(this.stockID !== undefined)
                        this.stock = this.stocksService.getStockById(this.stockID);
                }
            )
         }
    
         modifyStock(): void{
            
         }
}