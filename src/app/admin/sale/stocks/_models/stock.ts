export class Stock{
    
    constructor(
        public stockID: number,
        public name: string,
        public symbol: string,
        public description: string,
        public mainStock: string,
        public externalStore: number,
        public skipSynch: boolean,
    ){}
    }