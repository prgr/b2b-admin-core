import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../../shared/_services/shared.service';
import { LoyaltyService } from '../_services/loyalty.service';
import { Loyalty } from './/../_models/loyalty';

@Component({
    templateUrl: 'admin.modify-loyalty-sale.compontent.html'
})

export class AdminModifyLoyaltySaleComponent {
    constructor(
        private translate: TranslateService,
        private sharedService: SharedService,
        private route: ActivatedRoute,
        private loyaltyService: LoyaltyService,
    ) { }
    public loyalty: Loyalty;
    private loyaltyID: number;

    ngOnInit() {
        //language
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        this.route.params.subscribe(
            (params: any) => {
                this.loyaltyID = params['id']
                //get static page
                if(this.loyaltyID !== undefined)
                    this.loyalty = this.loyaltyService.getLoyaltyById(this.loyaltyID);
            }
        )
     }

     modifyLoyalty(): void{
        
     }
}