import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../../shared/_services/shared.service';
import { PromotionCodesService } from '../_services/promotion-codes.service';
import { PromotionCode } from '../_models/promotionCode';

@Component({
    templateUrl: 'admin.modify-code-sale.component.html'
})

export class AdminModifyCodeSaleComponent {
    constructor(
        private translate: TranslateService,
        private sharedService: SharedService,
        private route: ActivatedRoute,
        private promotionCodeService: PromotionCodesService,
    ) { }
    public promotionCode: PromotionCode;
    private promotionCodeID: number;

    ngOnInit() {
        //language
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        this.route.params.subscribe(
            (params: any) => {
                this.promotionCodeID = params['id']
                //get static page
                if(this.promotionCodeID !== undefined)
                    this.promotionCode = this.promotionCodeService.getPromotionCodeById(this.promotionCodeID);
            }
        )
     }

     modifyPromotionCode(): void{
        
     }
}