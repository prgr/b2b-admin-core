import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../shared/_services/shared.service';
import { HelperModule } from '../../../../assets/js/helpers';
import { Product } from '../products/_models/product';
import { AdminProductsSaleService } from '../products/_services/admin.products-sale.service';

@Component({
    templateUrl: 'admin.modify-product-sale.component.html'
})

export class AdminModifyProductSaleComponent implements OnInit {
    constructor(private sharedService: SharedService, private translate: TranslateService, private route: ActivatedRoute, private productsService: AdminProductsSaleService) { }
    helpers = new HelperModule.HelpersMethods();

    public product: Product;
    private productID: number;

    ngOnInit() {
        //initial multi languages
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        //product
        this.route.params.subscribe(
            (params: any) => {
                this.productID = params['id']
                //get static page
                if(this.productID !== undefined)
                    this.product = this.productsService.getProductById(this.productID);
            }
        )
     }

     uploadImagesPreview(ul: HTMLUListElement, input: HTMLInputElement): void{
        this.helpers.uploadImagePreview(ul, input);
     }
}