export class Product{

constructor(
    public productID: number,
    public name: string,
    public image: string,
    public symbol: string,
    public unitName: string,
    public VAT: number,
    public retailPrice: number,
    public priceNetto: number,
    public priceBrutto: number,
    public discount: number,
    public availability: number,
    public isAvailable: boolean,
    public sale: boolean,
    public pnew: boolean,
    public promo: boolean,
    public quantity?: number,
    public discountInit?: number
){}
}