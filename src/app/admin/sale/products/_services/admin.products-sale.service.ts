import { Injectable } from '@angular/core';
import { Product } from '../_models/product';

@Injectable()
export class AdminProductsSaleService {

    constructor() { }

    public products: Product[] = this.getProducts();

    getProducts(){
        return this.generateProducts();
    }

    getProductById(id: number): Product{
        return this.products.find(x => x.productID == id);
    }

    generateProducts(): Product[]{
        const products: Product[] = [];
        for(let i = 1; i < 500; i++){
            const price_netto = i + 0.99;
            const vat = price_netto * (23 / 100);
            const price_brutto = (price_netto + vat).toFixed(2);

            //image
            let image = 'product_sample';
            if(i < 6)
                image = `product_${i}_preview_image`;

            const product = new Product(parseInt(i.toString()), `Product ${i} angular 4 b2b`, image, `111${i}`, 'szt.', 23, price_netto, price_netto, parseFloat(price_brutto), 0, 500, true, false, false, false);
            products.push(product);
        }
        return products;
    }

    filterProducts(value: string): Product[]{
        return this.products.filter(product => product.name.toLowerCase().includes(value) || product.symbol.toLowerCase().includes(value));
    }

    changeProductStatus(id: number, status: string): boolean{
        const product = this.getProductById(id);
        let product_status: boolean;
        switch (status) {
            case 'sale':
                product.sale = !product.sale;
                product_status = product.sale;    
                break;
            case 'promo':
                product.promo = !product.promo;
                product_status = product.promo;
                break;
            case 'new':
                product.pnew = !product.pnew;
                product_status = product.pnew;
                break;
            default:
                break;   
        }
        return product_status; 
    }

    sortProducts(products: Product[], sortType: string): Product[]{
        switch (sortType) {
            case 'priceNetto':
                products.sort(x => x.priceNetto);
                console.log(products);
            break;
            case 'priceBrutto':
                products.sort(x => x.priceBrutto);
            break;
            case 'retailPrice':
                products.sort(x => x.retailPrice);
            break;
            case 'availability':
                products.sort(x => x.availability);
            default:
                break;
        }
        return products;
    }
}