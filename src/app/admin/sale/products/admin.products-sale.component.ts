import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Product } from '../products/_models/product';
import { PaginationService } from '../../_services/pagination.service';
import { AdminProductsSaleService } from './_services/admin.products-sale.service';
import { MdSnackBar } from '@angular/material';
import { ImagePreviewModule } from '../../../../assets/js/extensions/image-preview';

@Component({
    templateUrl: 'admin.products-sale.component.html',
    providers: [ AdminProductsSaleService, PaginationService ],
})

export class AdminProductsSaleComponent implements OnInit {
  constructor(private productsSalesService: AdminProductsSaleService, private paginationService: PaginationService, private tdr: ChangeDetectorRef, public snackBar: MdSnackBar) { }
  //products
  public products: Product[];
  //cart button
  public cartButton: boolean = false;
  //sort
  public sortType: string = '';
  //settings
  public settings: any = '';
  //pagination
  private allItems: any[];
  //pager
  pager: any = {};
  //first page
  public firstPage: boolean = false;
  public lastPage: number = 0;
  public currentPage: number = 1;
  //page items
  pagedItems: any[];
  //custom methods
  imagePreviewExt = new ImagePreviewModule.ImagePreview();

    ngOnInit() { 
        this.products = this.productsSalesService.products;
        // initialize products table and set page 1
        this.allItems = this.products;
        this.setPage(1);
        this.lastPage = this.pager.finalPage;
    }

    filterTable(value: string) {
        const filtered_products = this.productsSalesService.filterProducts(value.toLowerCase());
        this.products = filtered_products;
        this.allItems = filtered_products;
        this.setPage(1);
    }

    sort(sortColumn: string): void{
        this.sortType = sortColumn;
        this.products = this.productsSalesService.sortProducts(this.products, sortColumn);
    }

    searchListener(value: string, keyCode: number): void{
    if(keyCode == 13)
        this.filterTable(value);
    else if(value == '')
        this.filterTable(value);
    else
        return;
    }

    imagePreview(element: HTMLImageElement){
        this.imagePreviewExt.imagePreview(element, true, true)
    }

    setPage(page: number) {
        //active get back to first page button
        this.firstPage = page >= 7 ? true : false;
        if (page < 1 || page > this.pager.totalPages)
            return;
        // get pager object from service
        this.pager = this.paginationService.getPager(this.allItems.length, page);
        // get current page of items
        this.products = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
        this.products = this.productsSalesService.sortProducts(this.products, this.sortType);
        this.currentPage = page;
    }

    changeProductStatus(productID: number, status: string){
        const result: boolean = this.productsSalesService.changeProductStatus(productID, status);
        let status_text;
        switch (status) {
            case 'sale':
                status_text = 'Wyprzedaż';
                break;
            case 'promo':
                status_text = 'Promocja';
                break;
            case 'new':
                status_text = 'Nowość';
                break;
            default:
                break;
        }
        //if(result)
            //this.statusInfo('Status produktu dodany jako:', status_text, 'success');
        //else
            //this.statusInfo('Status produktu usuniety:', status_text, 'alert');
    }

    statusInfo(message: string, value: string, type: string) {
        this.snackBar.open(message, value, {
            duration: 2000,
            extraClasses: [type]
        });
    }
}