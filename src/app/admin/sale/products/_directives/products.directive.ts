import { Directive, HostListener, ElementRef, Renderer2 } from '@angular/core';

/**
* Allows the sidebar to be toggled via click.
*/
@Directive({
  selector: '[sortToggle]'
})
export class SortColumnToggleDirective {
    private el: ElementRef;
    constructor(elem: ElementRef, renderer: Renderer2) {
        this.el = elem;
     }

  @HostListener('click', ['$event'])
  toggle($event: any) {
    $event.preventDefault();
    this.el.nativeElement.classList.toggle('sort_active');
    let currentChevron = this.el.nativeElement.children[0].innerHTML;
    this.el.nativeElement.children[0].innerHTML = currentChevron == 'keyboard_arrow_down' ? 'keyboard_arrow_up' : 'keyboard_arrow_down';
  }
}

export const PRODUCTS_DIRECTIVES = [
    SortColumnToggleDirective
];
