import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LanguagesService } from '../../shared/_services/languages.service';
import { SharedService } from '../../shared/_services/shared.service';


@Component({
    templateUrl: 'admin.dashboard.component.html'
})

export class AdminDashboardComponent implements OnInit {
    constructor(
        private translate: TranslateService,
        private langService: LanguagesService,
        private sharedService: SharedService,
    ) { }

    ngOnInit(){
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
    }

    
}