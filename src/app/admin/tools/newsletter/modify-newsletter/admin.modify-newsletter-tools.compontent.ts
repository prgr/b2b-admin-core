import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../../shared/_services/shared.service';
import { NewsletterService } from '../_services/newsletter.service';
import { Newsletter } from '../_models/newsletter';

@Component({
    templateUrl: 'admin.modify-newsletter-tools.compontent.html'
})

export class AdminModifyNewsletterToolsComponent {
        constructor(
            private translate: TranslateService,
            private sharedService: SharedService,
            private route: ActivatedRoute,
            private newsletterService: NewsletterService,
        ) { }
        public newsletter: Newsletter;
        private newsletterID: number;
    
        ngOnInit() {
            //language
            let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
            this.translate.use(lang);
            this.route.params.subscribe(
                (params: any) => {
                    this.newsletterID = params['id']
                    //get static page
                    if(this.newsletterID !== undefined)
                        this.newsletter = this.newsletterService.getNewsletterById(this.newsletterID);
                }
            )
         }
    
         modifyNewsletter(): void{
            
         }
}