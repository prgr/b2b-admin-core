import { Injectable } from '@angular/core';
import { Newsletter } from '../_models/newsletter';

@Injectable()
export class NewsletterService {
    public newsletters: Newsletter[] = [
        new Newsletter(1, 'hero', 'hero', '<h1>hero</h1>'),
        new Newsletter(2, 'alfa', 'alfa', '<h1>alfa</h1>'),
        new Newsletter(3, 'ranger', 'ranger', '<h1>ranger</h1>')
    ]
    constructor() { }

    getNewsletterById(id: number): Newsletter{
        return this.newsletters.find(x => x.newsletterID == id);
    }
}