export class Newsletter{
    
    constructor(
        public newsletterID: number,
        public name: string,
        public subject: string,
        public content: string
    ){}
    }