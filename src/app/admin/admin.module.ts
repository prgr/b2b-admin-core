import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { routing } from '../app.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { SettingsService, ToolsService } from './_services/index';
import { AdminDashboardComponent } from './dashboard/admin.dashboard.component';
import { MaterialDesignModule } from '../shared/_material-design/material-design.module';
//admin configurations
import {SettingsComponent, AdminGeneralConfigurationComponent, AdminContactConfigurationComponent,
        AdminStocksConfigurationComponent, AdminClientsConfigurationComponent, AdminPaymentsConfigurationComponent,
        AdminShippingConfigurationComponent, AdminStatusConfigurationComponent, AdminNotificationsConfigurationComponent,
        AdminEmailConfigurationComponent, AdminNewsletterConfigurationComponent, AdminPriceListsConfigurationComponent,
        EventLogComponent, GeneralService, AdminModifyPriceListConfigurationComponent, PriceListService } from './configuration/index';
//admin tools
import { AdminComplaintsComponent, AdminNewsletterToolsComponent, AdminModifyNewsletterToolsComponent, NewsletterService ,
         AdminNewsletterAdressesToolsComponent} from './tools/index';
//admin sale
import { AdminClientsSaleComponent, AdminOrdersSaleComponent, AdminPromotionCodesSaleComponent, AdminStocksSaleComponent,
         AdminTradersSaleComponent, AdminLoyaltySaleComponent, AdminProductsSaleComponent, AdminProductsSaleService,
         AdminModifyProductSaleComponent, AdminModifyCodeSaleComponent, PromotionCodesService, AdminModifyStockSaleComponent, StocksService,
         AdminProductAliasesSaleComponent, ProductAliasService, AdminModifyAliasSaleComponent, AdminCategoriesSortSaleCompontent, CategoriesSortService,
         AdminModifyTraderSaleComponent, TraderService, LoyaltyService, AdminModifyLoyaltySaleComponent } from './sale/index';
//admin content-management
import { AdminMenuContentManagementComponent, AdminNewsContentManagementComponent, AdminStaticPagesContentManagementComponent,
         AdminModifyPageContentManagementComponent, StaticPageService, AdminSiteElementsContentManagementComponent, SiteElementService,
         AdminModifyElementContentManagementComponent, NewsService, AdminModifyNewsManagementComponent,
         AdminBannersContentManagementComponent, AdminModifyBannerContentManagementComponent, BannerService, MenuService,
         AdminModifyMenuContentManagementComponent, AdminModifySectionContentManagementComponent, SectionService } from './content-management/index';

//ckeditor
import { CKEditorModule } from 'ng2-ckeditor';
//directives
import { PRODUCTS_DIRECTIVES } from './sale/products/_directives/products.directive';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
    return new TranslateHttpLoader(http, "./assets/data/languages/", ".json");
  }

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        routing,
        MaterialDesignModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        CKEditorModule
    ],
    declarations: [
        AdminDashboardComponent,
        //admin configurations
        SettingsComponent,
        AdminGeneralConfigurationComponent,
        AdminContactConfigurationComponent,
        AdminPriceListsConfigurationComponent,
        AdminStocksConfigurationComponent,
        AdminClientsConfigurationComponent,
        AdminPaymentsConfigurationComponent,
        AdminShippingConfigurationComponent,
        AdminStatusConfigurationComponent,
        AdminNotificationsConfigurationComponent,
        AdminEmailConfigurationComponent,
        AdminNewsletterConfigurationComponent,
        EventLogComponent,
        AdminModifyPriceListConfigurationComponent,
        //admin tools
        AdminComplaintsComponent,
        AdminNewsletterToolsComponent,
        AdminModifyNewsletterToolsComponent,
        AdminNewsletterAdressesToolsComponent,
        //admin sale
        AdminClientsSaleComponent,
        AdminOrdersSaleComponent,
        AdminPromotionCodesSaleComponent,
        AdminStocksSaleComponent,
        AdminTradersSaleComponent,
        AdminLoyaltySaleComponent,
        AdminProductsSaleComponent,
        AdminModifyProductSaleComponent,
        AdminModifyCodeSaleComponent,
        AdminModifyStockSaleComponent,
        AdminProductAliasesSaleComponent,
        AdminModifyAliasSaleComponent,
        AdminCategoriesSortSaleCompontent,
        AdminModifyTraderSaleComponent,
        AdminModifyLoyaltySaleComponent,
        //admin content management
        AdminNewsContentManagementComponent,
        AdminStaticPagesContentManagementComponent,
        AdminModifyPageContentManagementComponent,
        AdminSiteElementsContentManagementComponent,
        AdminModifyElementContentManagementComponent,
        AdminModifyNewsManagementComponent,
        AdminBannersContentManagementComponent,
        AdminModifyBannerContentManagementComponent,
        AdminMenuContentManagementComponent,
        AdminModifyMenuContentManagementComponent,
        AdminModifySectionContentManagementComponent,
        PRODUCTS_DIRECTIVES
    ],
    providers: [
        SettingsService,
        ToolsService,
        AdminProductsSaleService,
        StaticPageService,
        SiteElementService,
        SectionService,
        NewsService,
        BannerService,
        MenuService,
        PromotionCodesService,
        StocksService,
        ProductAliasService,
        CategoriesSortService,
        TraderService,
        LoyaltyService,
        NewsletterService,
        GeneralService,
        PriceListService
    ]
})
export class AdminModule { }
