import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LanguagesService } from '../../../../shared/_services/languages.service';
import { SharedService } from '../../../../shared/_services/shared.service';
import { SiteElement } from '../_models/site-element';
import { SiteElementService } from '../_services/site-element.service';

@Component({
    templateUrl: 'admin.modify-element-content-management.component.html'
})

export class AdminModifyElementContentManagementComponent implements OnInit {
    constructor(
        private translate: TranslateService,
        private sharedService: SharedService,
        private siteElementService: SiteElementService,
        private route: ActivatedRoute
    ) { }
    public siteElement: SiteElement;
    private siteElementID: number;

    ngOnInit() {
        //language
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        this.route.params.subscribe(
            (params: any) => {
                this.siteElementID = params['id']
                //get static page
                if(this.siteElementID !== undefined)
                    this.siteElement = this.siteElementService.getSiteElementById(this.siteElementID);
            }
        )
     }
     modifyElementContent(){}
}