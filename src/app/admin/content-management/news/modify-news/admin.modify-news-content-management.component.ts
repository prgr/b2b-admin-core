import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LanguagesService } from '../../../../shared/_services/languages.service';
import { SharedService } from '../../../../shared/_services/shared.service';
import { News } from '../_models/news';
import { NewsService } from '../_services/news.service';

@Component({
    templateUrl: 'admin.modify-news-content-management.component.html'
})

export class AdminModifyNewsManagementComponent implements OnInit {
    constructor(
        private translate: TranslateService,
        private sharedService: SharedService,
        private newsService: NewsService,
        private route: ActivatedRoute
    ) { }
    public news: News;
    private newsID: number;

    ngOnInit() {
        //language
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        this.route.params.subscribe(
            (params: any) => {
                this.newsID = params['id']
                //get static page
                if(this.newsID !== undefined)
                    this.news = this.newsService.getNewsById(this.newsID);
            }
        )
     }
     modifyNews(){
         
     }
}