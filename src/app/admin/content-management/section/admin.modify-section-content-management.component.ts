import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../shared/_services/shared.service';
import { SectionService } from './_services/section.service';
import { Section } from './_models/section';

@Component({
    templateUrl: 'admin.modify-section-content-management.component.html'
})

export class AdminModifySectionContentManagementComponent implements OnInit {
    constructor(
        private translate: TranslateService,
        private sharedService: SharedService,
        private route: ActivatedRoute,
        private sectionService: SectionService,
    ) { }
    public section: Section;
    private sectionID: number;

    ngOnInit() {
        //language
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        this.route.params.subscribe(
            (params: any) => {
                this.sectionID = params['id']
                //get static page
                if(this.sectionID !== undefined)
                    this.section = this.sectionService.getSectionById(this.sectionID);
            }
        )
     }

     modifySection(): void{
        
     }
}