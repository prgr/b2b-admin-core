import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuService } from '../_services/menu.service';
import { Menu } from '../_models/menu';

@Component({
    templateUrl: 'admin.modify-menu-content-management.component.html'
})

export class AdminModifyMenuContentManagementComponent implements OnInit {
    constructor(
        private route: ActivatedRoute,
        private menuService: MenuService
    ) { }
    public menu: Menu;
    private menuID: number;

    ngOnInit() {
        this.route.params.subscribe(
            (params: any) => {
                this.menuID = params['id']
                //get static page
                if(this.menuID !== undefined)
                    this.menu = this.menuService.getMenuById(this.menuID);
            }
        )
     }
     modifyMenu(){}
}