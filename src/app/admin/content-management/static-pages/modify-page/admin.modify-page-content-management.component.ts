import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LanguagesService } from '../../../../shared/_services/languages.service';
import { SharedService } from '../../../../shared/_services/shared.service';
import { StaticPage } from '../_models/static-page';
import { StaticPageService } from '../_services/static-page.service';

@Component({
    templateUrl: 'admin.modify-page-content-management.component.html'
})

export class AdminModifyPageContentManagementComponent implements OnInit {
    constructor(
        private translate: TranslateService,
        private sharedService: SharedService,
        private staticPageService: StaticPageService,
        private route: ActivatedRoute
    ) { }
    public staticPage: StaticPage;
    private staticPageID: number;

    ngOnInit() {
        //language
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        this.route.params.subscribe(
            (params: any) => {
                this.staticPageID = params['id']
                //get static page
                if(this.staticPageID !== undefined)
                    this.staticPage = this.staticPageService.getStaticPageById(this.staticPageID);
            }
        )
     }

     modifyStaticPage(): void{
        
     }
}