import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from '../../../shared/_services/shared.service';

@Component({
    templateUrl: 'admin.static-pages-content-management.component.html'
})

export class AdminStaticPagesContentManagementComponent {
    constructor(
        private translate: TranslateService,
        private sharedService: SharedService,
    ) { }

    ngOnInit() { 
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
    }
}