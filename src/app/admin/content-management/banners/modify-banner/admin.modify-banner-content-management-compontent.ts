import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BannerService } from '../_services/banner.service';
import { Banner } from './../_models/banner';


@Component({
    templateUrl: 'admin.modify-banner-content-management-compontent.html'
})

export class AdminModifyBannerContentManagementComponent implements OnInit {
    constructor(
        private bannerService: BannerService,
        private route: ActivatedRoute
    ) { }
    public banner: Banner;
    private bannerID: number;

    ngOnInit() {
        //language
        this.route.params.subscribe(
            (params: any) => {
                this.bannerID = params['id']
                //get static page
                if(this.bannerID !== undefined)
                    this.banner = this.bannerService.getBannerById(this.bannerID);
            }
        )
     }

     modifyBanner(){
         
     }
}