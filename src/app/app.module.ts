import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './app.routing';
import { HttpModule, Http } from '@angular/http';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { AppComponent } from './app.component';
import { MaterialDesignModule } from './shared/_material-design/material-design.module';
import { SharedService } from './shared/_services/shared.service';
import { LoginComponent, PasswordReminderComponent, NewsCompontent, AuthService } from './account/index';
import { LanguagesService } from './shared/_services/languages.service';
import { DialogMessageComponent } from './shared/_popups/dialog.message.component';
import { DialogConfirmComponent } from './shared/_popups/confirm/dialog.confirm.component';
import { PopupsService } from './shared/_services/popups.service';
import { HomeComponent } from './home/home.component';
import { AdminModule } from './admin/admin.module';
import { ClientSidebarComponent} from './layout/sidebar/client-sidebar.component';
import { ClientHeaderComponent } from './layout/header/client-header.component';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/_directives/sidebar.directive';
import { HEADER_DIRECTIVES } from './shared/_directives/header.directive';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, "./assets/data/languages/", ".json");
}

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    MaterialDesignModule,
    //admin
    AdminModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  declarations: [
    AppComponent,
    //home
    HomeComponent,
    //account
    LoginComponent,
    PasswordReminderComponent,
    NewsCompontent,
    //popups
    DialogMessageComponent,
    DialogConfirmComponent,
    ClientSidebarComponent,
    ClientHeaderComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    HEADER_DIRECTIVES
  ],
  providers: [
    HttpClient,
    SharedService,
    AuthService,
    LanguagesService,
    PopupsService
  ],
  entryComponents: [DialogMessageComponent, DialogConfirmComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
