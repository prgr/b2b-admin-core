import { Component, ViewChild, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
// import { LanguagesService } from '../../shared/_services/languages.service';
import { SharedService } from '../../shared/_services/shared.service';
import { AuthService } from '../../account/_services/auth.service';

@Component({
    selector: 'client-header',
    templateUrl: 'client-header.component.html'
})

export class ClientHeaderComponent implements OnInit {
    @ViewChild('header') header;
    public showMenu: boolean = true;
    public settings: any = '';
    //public toggleMenu: boolean = true;
    constructor(private translate: TranslateService, private sharedService: SharedService, private authenticationService: AuthService) {
     }
    hideMenu(){
        this.showMenu = !this.showMenu;
    }
    ngOnInit(){
        //initial multi languages
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        //initial settings
        this.settings = this.sharedService.globalSettings == null || this.sharedService.globalSettings == undefined 
                    ? this.sharedService.getSettings().subscribe(settings => this.settings = settings) : this.sharedService.globalSettings;
        this.settings = this.sharedService.globalSettings;
    }
    logout(){
        //this.popupsService.dialog('natural', 'Konczenie sesji.', 'info_outline', 'home');
        //this.dialog.openDialog();
        this.authenticationService.logout();
    }
}