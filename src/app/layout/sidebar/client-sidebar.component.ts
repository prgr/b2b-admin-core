import { Component, ViewChild, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
// import { LanguagesService } from '../../shared/_services/languages.service';
import { SharedService } from '../../shared/_services/shared.service';

@Component({
    selector: 'client-sidebar',
    templateUrl: 'client-sidebar.component.html'
})

export class ClientSidebarComponent implements OnInit {
    @ViewChild('sideMenu') sideMenu;
    public showMenu: boolean = true;
    public settings: any = '';
    //public toggleMenu: boolean = true;
    constructor(private translate: TranslateService, private sharedService: SharedService) {
     }
    hideMenu(){
        this.showMenu = !this.showMenu;
    }
    ngOnInit(){
        //initial multi languages
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        this.translate.use(lang);
        //initial settings
        this.settings = this.sharedService.globalSettings == null || this.sharedService.globalSettings == undefined 
                    ? this.sharedService.getSettings().subscribe(settings => this.settings = settings) : this.sharedService.globalSettings;
        this.settings = this.sharedService.globalSettings;
    }

    toggleSubMenu(subMenu){
        subMenu.classList.contains('show') ? subMenu.classList.remove('show') : subMenu.classList.add('show');
    }
}