import { Directive, HostListener, ElementRef, Renderer } from '@angular/core';

/**
* Allows the sidebar to be toggled via click.
*/
@Directive({
  selector: '[dropdownToggle]'
})
export class DropdownToggleDirective {
    private el: ElementRef;
    private listenFunc: Function;
    constructor(elem: ElementRef, public renderer: Renderer) {
        this.el = elem;
     }

  @HostListener('click', ['$event'])
  toggle($event: any) {
    $event.preventDefault();
    this.el.nativeElement.parentElement.children[1].classList.toggle('show');


    //add html event focusout on cart items element
    this.listenFunc = this.renderer.listen(this.el.nativeElement, 'focusout', (event) => {
      //get what was clicked
      let clicked_out = event.relatedTarget
      //check if element what was clicked is button see more and if it is then prevent from closing cart items element
      if(event.relatedTarget !== null && clicked_out.classList.contains('cart-check') || event.relatedTarget !== null && clicked_out.classList.contains('cart-product-link')){
          if(clicked_out.classList.contains('cart-check') || clicked_out.classList.contains('cart-product-link'))
              event.preventDefault();
      }
      //else
          //this.showCart = false;
    });


  }
}

export const HEADER_DIRECTIVES = [
    DropdownToggleDirective
];
