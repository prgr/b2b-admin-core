import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MdDialog } from '@angular/material';
import { Message } from '../_models/message';
import { PopupsService } from '../_services/popups.service';

@Component({
    selector: 'dialog-message',
    templateUrl: 'dialog.message.component.html',
    providers: [DialogMessageComponent]
})
@Injectable()
export class DialogMessageComponent { 
constructor(public dialog: MdDialog, public popupsService: PopupsService, private router: Router) { }

dialogMessage: Message = this.popupsService.message;

    openDialog(){
        //open dialog
        let _dialog = this.dialog.open(DialogMessageComponent);
        this.dialogAutoClose(_dialog);
    }

    dialogAutoClose(_dialog){
        setTimeout(function(){
            _dialog.close();
        }, 112000)
        const path = this.popupsService.message.navigate;
        if(path != '' && path != null)
            this.router.navigate([path]);
    }
}
