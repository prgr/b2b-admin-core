import {Component, Injectable} from '@angular/core';
import {MdDialog} from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  templateUrl: 'dialog.confirm.component.html',
})
@Injectable()
export class DialogConfirmComponent {
  constructor(public dialog: MdDialog, private translate: TranslateService) {}
  confirmDialog() {
    return this.dialog.open(DialogConfirmComponent);
    //return result;
  }
}