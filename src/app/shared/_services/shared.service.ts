import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import {Observable, Observer} from 'rxjs/Rx';

@Injectable()
export class SharedService {
    public globalSettings: any | null;
    public language: any | null;

    constructor(private http: Http, private router: Router) {
        //this.globalVar = this.getHero();
    }

    /*
    getHero(){
        console.log('INJECTED');
        return this.http.get('assets/data/settings/test.json').map(res => res.json()).subscribe(
            v => {
            console.log(v);
            this.globalVar = v;
            },
            err => {
            console.log(`Error: ${err}`);
            }
        )
    }
    */

    getUsedLanguage(){
        //get local lang
        const local_lang = localStorage.getItem('lang');
        //check if language is already choosen if not set default language to pl
        return local_lang !== null || undefined ? local_lang : 'pl';
    }

    setSettings(){
        this.http.get('assets/data/settings/settings.json').map(response => response.json()).subscribe(setts => {
            //set settings variable to use it instead of calling http get method
            if(this.globalSettings == null || this.globalSettings == undefined)
                this.globalSettings = setts;
            //return setts;  
        });
    }

    getSettings(){
        //this.router.navigate(['customer/clients-list']);
        this.setSettings();
            return this.http.get('assets/data/settings/settings.json')
              .map(res => res.json())
    }

    //testing
    getSettingsHero(){
        return this.http.get('assets/data/settings/settings.json').map(response => response.json()).subscribe(setts => {
            //set settings variable to use it instead of calling http get method
            if(this.globalSettings == null || this.globalSettings == undefined){
                this.globalSettings = setts;
            } 
        }).unsubscribe();
    }
    //testing
    getSettingsMag(){
        return this.http.get('assets/data/settings/settings.json').map(response => response.json() as JSON).toPromise();
    }
}