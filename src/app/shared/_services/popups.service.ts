import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Message } from '../_models/message';
import { Status } from '../_models/status';
import { DialogMessageComponent } from '../_popups/dialog.message.component';

@Injectable()
export class PopupsService {
    public message: Message;
    public status: Status;

    dialog(type: string, text: string, icon: string, navigate: string): void{
        this.message = new Message(type, text, icon, navigate);
    }
}