import { Component } from '@angular/core';
import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { TranslateService } from '@ngx-translate/core';
import { SharedService } from './shared/_services/shared.service';
import { DialogMessageComponent } from './shared/_popups/dialog.message.component';
import { PopupsService } from './shared/_services/popups.service';
import { AuthService } from './account/_services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [ DialogMessageComponent ]
})
export class AppComponent {
  public activateLinks: boolean;
  title = 'app';
  public roles = [
    { id: 1, 'viewValue': 'admin'},
    { id: 2, 'viewValue': 'customer'},
    { id: 3, 'viewValue': 'seller'}
  ]
  constructor(public translate: TranslateService, public sharedService: SharedService, private popupsService: PopupsService, 
              private dialog: DialogMessageComponent, private authenticationService: AuthService,){
        translate.addLangs(['en', 'pl']);
        let lang = this.sharedService.language == null || undefined ? this.sharedService.getUsedLanguage() : this.sharedService.language;
        translate.use(lang);  
  }
    canActivateLinks(){
      //verify if currentUser and token fits
      const currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if(currentUser !== null){
            const token = currentUser.token
            this.activateLinks = token == 'token12345' ? true : false;
            return this.activateLinks;
      }
    }

    logout(){
        //this.popupsService.dialog('natural', 'Konczenie sesji.', 'info_outline', 'home');
        //this.dialog.openDialog();
        this.authenticationService.logout();
    }
}
