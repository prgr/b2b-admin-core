//angular imports
import { Routes, RouterModule } from '@angular/router';
//home
import { HomeComponent } from './home/home.component';
//accounts components
import { LoginComponent, PasswordReminderComponent, NewsCompontent } from './account/index';
//admin
import { AdminDashboardComponent } from './admin/dashboard/admin.dashboard.component';
import { ClientSidebarComponent} from './layout/sidebar/client-sidebar.component';
import { ClientHeaderComponent } from './layout/header/client-header.component';
//admin configurations
import { SettingsComponent, AdminGeneralConfigurationComponent, AdminContactConfigurationComponent,
         AdminStocksConfigurationComponent, AdminClientsConfigurationComponent, AdminPaymentsConfigurationComponent,
         AdminShippingConfigurationComponent, AdminStatusConfigurationComponent, AdminNotificationsConfigurationComponent,
         AdminEmailConfigurationComponent, AdminNewsletterConfigurationComponent, AdminPriceListsConfigurationComponent,
         EventLogComponent, AdminModifyPriceListConfigurationComponent } from './admin/configuration/index';
//admin tools
import { AdminComplaintsComponent, AdminNewsletterToolsComponent, AdminModifyNewsletterToolsComponent,
         AdminNewsletterAdressesToolsComponent } from './admin/tools/index';
//admin sale
import { AdminClientsSaleComponent, AdminOrdersSaleComponent, AdminPromotionCodesSaleComponent, AdminStocksSaleComponent,
         AdminTradersSaleComponent, AdminLoyaltySaleComponent, AdminProductsSaleComponent, AdminModifyProductSaleComponent, AdminModifyCodeSaleComponent,
         AdminModifyStockSaleComponent, AdminProductAliasesSaleComponent, AdminModifyAliasSaleComponent, AdminCategoriesSortSaleCompontent,
         AdminModifyTraderSaleComponent, AdminModifyLoyaltySaleComponent } from './admin/sale/index';
//admin content-management
import { AdminMenuContentManagementComponent, AdminNewsContentManagementComponent, AdminStaticPagesContentManagementComponent,
         AdminModifyPageContentManagementComponent, AdminSiteElementsContentManagementComponent, AdminModifyElementContentManagementComponent,
         AdminModifyNewsManagementComponent, AdminBannersContentManagementComponent, AdminModifyBannerContentManagementComponent,
         AdminModifyMenuContentManagementComponent, AdminModifySectionContentManagementComponent  } from './admin/content-management/index';


const appRoutes: Routes = [
        //{ path: 'home', component: HomeComponent},
        //account
        { path: 'login', component: LoginComponent },
        { path: 'password-remind', component: PasswordReminderComponent },
        //any other paths
        { path: '', component: LoginComponent},
        //admin routes
        { path: 'admin', children:[
            { path: 'dashboard', component: AdminDashboardComponent},
            { path: '' , component: ClientSidebarComponent, outlet: 'sidebar'},
            { path: '' , component: ClientHeaderComponent, outlet: 'header'},
            { path: 'news', component: NewsCompontent },
      //admin conigurations
     { path: 'configuration', children: [
        { path: 'settings', component: SettingsComponent},
        { path: 'general', component: AdminGeneralConfigurationComponent},
        { path: 'contact', component: AdminContactConfigurationComponent},
        { path: 'price-lists', component: AdminPriceListsConfigurationComponent },
        { path: 'stocks', component: AdminStocksConfigurationComponent},
        { path: 'clients', component: AdminClientsConfigurationComponent},
        { path: 'payments', component: AdminPaymentsConfigurationComponent },
        { path: 'shipping', component: AdminShippingConfigurationComponent },
        { path: 'status', component: AdminStatusConfigurationComponent },
        { path: 'notifications', component: AdminNotificationsConfigurationComponent},
        { path: 'email', component: AdminEmailConfigurationComponent},
        { path: 'newsletter', component: AdminNewsletterConfigurationComponent },
        { path: 'eventlog', component: EventLogComponent },
        { path: 'modify-price-list', component: AdminModifyPriceListConfigurationComponent },
        { path: 'modify-price-list/:id', component: AdminModifyPriceListConfigurationComponent },
     ]},
     //admin tools
     { path: 'tools', children: [
        { path: 'complaints', component: AdminComplaintsComponent },
        { path: 'newsletter', component: AdminNewsletterToolsComponent },
        { path: 'modify-newsletter', component: AdminModifyNewsletterToolsComponent },
        { path: 'modify-newsletter/:id', component: AdminModifyNewsletterToolsComponent },
        { path: 'newsletter-addresses', component: AdminNewsletterAdressesToolsComponent }
     ]},
     //admin sale
     { path: 'sale', children: [
         { path: 'clients', component: AdminClientsSaleComponent },
         { path: 'orders', component: AdminOrdersSaleComponent },
         { path: 'promotion-codes', component: AdminPromotionCodesSaleComponent },
         { path: 'stocks', component: AdminStocksSaleComponent },
         { path: 'modify-stock', component: AdminModifyStockSaleComponent },
         { path: 'modify-stock/:id', component: AdminModifyStockSaleComponent },
         { path: 'traders', component: AdminTradersSaleComponent },
         { path: 'loyalty', component: AdminLoyaltySaleComponent },
         { path: 'products', component: AdminProductsSaleComponent },
         { path: 'modify-product', component: AdminModifyProductSaleComponent},
         { path: 'modify-code', component: AdminModifyCodeSaleComponent },
         { path: 'modify-code/:id', component: AdminModifyCodeSaleComponent },
         { path: 'product-aliases', component: AdminProductAliasesSaleComponent },
         { path: 'modify-alias', component: AdminModifyAliasSaleComponent },
         { path: 'modify-alias/:id', component: AdminModifyAliasSaleComponent },
         { path: 'categories-sort', component: AdminCategoriesSortSaleCompontent },
         { path: 'modify-trader', component: AdminModifyTraderSaleComponent },
         { path: 'modify-trader/:id', component: AdminModifyTraderSaleComponent },
         { path: 'modify-loyalty', component: AdminModifyLoyaltySaleComponent },
         { path: 'modify-loyalty/:id', component: AdminModifyLoyaltySaleComponent },
     ]},
     { path: 'content-management', children: [
         { path: 'news', component: AdminNewsContentManagementComponent },
         { path: 'add-news', component: AdminModifyNewsManagementComponent },
         { path: 'edit-news/:id', component: AdminModifyNewsManagementComponent },
         { path: 'static-pages', component: AdminStaticPagesContentManagementComponent },
         { path: 'add-page', component: AdminModifyPageContentManagementComponent },
         { path: 'edit-page/:id', component: AdminModifyPageContentManagementComponent },
         { path: 'site-elements', component: AdminSiteElementsContentManagementComponent },
         { path: 'add-section', component: AdminModifySectionContentManagementComponent },
         { path: 'edit-section/:id', component: AdminModifySectionContentManagementComponent },
         { path: 'add-site-element', component: AdminModifyElementContentManagementComponent },
         { path: 'edit-site-element/:id', component: AdminModifyElementContentManagementComponent },
         { path: 'banners', component: AdminBannersContentManagementComponent },
         { path: 'add-banner', component: AdminModifyBannerContentManagementComponent },
         { path: 'edit-banner/:id', component: AdminModifyBannerContentManagementComponent },
         { path: 'menus', component: AdminMenuContentManagementComponent },
         { path: 'add-menu', component: AdminModifyMenuContentManagementComponent },
         { path: 'edit-menu/:id', component: AdminModifyMenuContentManagementComponent },
     ]}
  ]}
];

export const routing = RouterModule.forRoot(appRoutes);

// export const routing = RouterModule.forRoot([
//     admin_routes
// ]);