export module HelperModule {
    export interface HelpersMethods{
            //
    }
  export class HelpersMethods {
            /*
                --creates status message at the right bottom corner of the page
                --actually not used, replaced by snackbars
            */
            createStatusMessage(type: string, text: string): void {
                //create status wrapper
                const product_status_msg_wrapper = this.createHTMLElement('div', 'product-status-msg');
                //create message content
                const status_msg_content = this.createHTMLElement('div', 'product-msg-content type', null, type);
                //create status text
                const status_text = this.createHTMLElement('p', 'status-text', text);
                //add status text
                status_msg_content.appendChild(status_text);
                //add dialog msg content
                product_status_msg_wrapper.appendChild(status_msg_content);
                //finally add product status message to the document body
                document.body.appendChild(product_status_msg_wrapper);
                //product_status_msg_wrapper.className += ' hide';
                //remove message
                setTimeout(function(){
                    document.body.removeChild(product_status_msg_wrapper);
                }, 1500);
            }
            createHTMLElement(element: string, className: string, text?: string, type?: string){
                //create element
                const createdElement =  document.createElement(element);
                //add class name with and set type if its defined
                createdElement.className = type !== null && type !== undefined ? className + ' ' + type : className;
                //set element text if its defined
                if(text !== null && text !== undefined)
                    createdElement.innerText = text;
                //return
                return createdElement;
            }
            isNull(element: any): boolean{
                return element == null || element == undefined ? true : false;
            }
            classTimeOut(selector: string, className: string, time: number): void{
                let elements = document.querySelectorAll(selector);
                for (let i=0; i < elements.length; i++) {
                    setTimeout(function(){
                        elements[i].classList.remove(className);
                    }, time);   
                }
            }
            removeHTMLElement(selector: string) {
                const del = document.querySelector(selector);
                document.body.removeChild(del);
            }
            uploadImagePreview(ul: HTMLUListElement, input: HTMLInputElement): void{
                for (let i = 0; i < input.files.length; i++) {
                    const li = this.createHTMLElement('li', 'image_preview', '', '');
                    const image = this.createHTMLElement('img', '', '', '');
                    const image_path = window.URL.createObjectURL(input.files[i]);
                    image.setAttribute('src', image_path);
                    li.appendChild(image);
                    ul.appendChild(li);
                }
            }
        }
}